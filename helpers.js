import fs from "fs";
import {getDate, getHours} from "date-fns";

const checkEmoji = "\u{2705}"


const readListFromFile = () => {
    return JSON.parse(fs.readFileSync("HealthCare.txt", "utf8"))
}

let healthList = readListFromFile()

export const writeListToFile = () => {
    fs.writeFileSync("HealthCare.txt", JSON.stringify(healthList))
}


const isSeenHealthKey = (healthKey) => {
    const currIsSeen = healthList[healthKey].isSeen
    const currDate = getDate(new Date())
    const check = currIsSeen != currDate
    return check
}
const isTimeOverlapping = (healthKey) => {
    const currKeyTime = Number(healthKey)
    const currHours = getHours(new Date())
    const check = currHours >= currKeyTime
    return check
}

export const generateMessageData = () => {
    let sortedHealthKey = []
    Object.keys(healthList).forEach(healthKey => {
        if (isSeenHealthKey(healthKey) && isTimeOverlapping(healthKey)) {
            sortedHealthKey.push(healthKey)
        }
    })
    return sortedHealthKey
}

export const sendShowAll = async (chatId, bot) => {
    await bot.sendMessage(chatId, "Assistant: ", {
        "reply_markup": {
            "remove_keyboard": true,
            "inline_keyboard":
                [
                    [{
                        text: "Показать всe",
                        callback_data: `ShowAll`
                    }]
                ]
        }
    })
}

const setIsSeenToList = (healthKey) => {
    const today = getDate(new Date())
    healthList[healthKey].isSeen = today
    writeListToFile()
}

export const sendGeneratedMessage = async (chatId, healthKey, bot, showAll = false) => {
    const inlineKeyboard = generateInlineKeyboard(healthKey)
    if(!showAll){
        setIsSeenToList(healthKey)
    }
    await bot.sendMessage(chatId, healthList[healthKey].preMes, {
        "reply_markup": {
            "remove_keyboard": true,
            "inline_keyboard": inlineKeyboard
        }
    })
}

export const generateInlineKeyboard = (healthKey) => {
    const currentList = healthList[healthKey]
    const healthListCurrentList = currentList.list
    const healthListCheckItem = currentList.checkItem
    let healthListButtons = []
    healthListButtons = healthListCurrentList.map(item => {
        const isPressed = healthListCheckItem[item]
        return [{
            text: isPressed ? `${item} ${checkEmoji}` : item,
            callback_data: `${item}//${healthKey}`
        }]
    })
    return healthListButtons
}

export const isCheckCurrentDrug = (healthKey, currentDrug, today) => {
    return healthList[healthKey].checkItem[currentDrug] != today
}
export const setCheckToCurrentDrug = (healthKey, currentDrug, today) => {
    healthList[healthKey].checkItem[currentDrug] = today
}
export const checkIsShowAllPressed = async (data, chatId, bot) => {
    if (data.data === "ShowAll") {
        await sendAllListsWithCheck(chatId, bot)

        return true

    } else {
        return false
    }
}
export const sendAllListsWithCheck = async (chatId, bot) => {
    for await (let healthKey of Object.keys(healthList)) {
        await sendGeneratedMessage(chatId, healthKey, bot, true)
    }
}